import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		boolean fin=false;
		Scanner sc=new Scanner(System.in);
		ArrayList <Tarea> tareas=new ArrayList();
		HashMap <Integer, ArrayList<Detalle>> auxdetalles=new HashMap();
		GestorTareas gt=new GestorTareas();
		
		gt.conectar();
		tareas=gt.cargarDatos();
		
		
		Detalle detaux;
		Tarea aux;
		String opcion;
		String menu="\n\n1)Listar tareas"
				+"\n2)Nueva tarea"
				+"\n3)Nuevo detalle"
				+"\n4)Listar detalles de tarea"
				+"\n5)Ver todos los detalles"
				+"\n6)Realizar un detalle"
				+"\n7)Borrar tarea y sus detalles"
				+"\n8)Salir"
				+"\n";
		
		
		while(!fin) {
			
			System.out.println(menu);
			opcion=sc.nextLine();
			
			switch (opcion) {
			case "1": //Listar tareas
				System.out.println("Lista de tareas\n");
					tareas.stream().forEach(p->System.out.println(p.toString()));	
			
				break;
			case "2": //nueva tarea
				System.out.println("Introduzca nombre nueva tarea");
				aux=new Tarea(sc.nextLine());
				if(gt.nuevaTarea(aux))
				tareas.add(aux);
				
				break;
			case "3": //nuevo detalle
				System.out.println("Introduzca nombre de detalle");
				String nomdet=sc.nextLine();
				System.out.println("Introduzca ID de la tarea a la que pertenece");
				int iddet=sc.nextInt();
				sc.nextLine();
				if(iddet>tareas.size()) {
					System.out.println("Id de tarea fuera de rango");
					break;
				}else {
					detaux=new Detalle(iddet, nomdet, false);
					if(gt.nuevoDetalle(detaux)) {
						for(int i=0; i<tareas.size();i++) {
							if(tareas.get(i).getId()==iddet) {
								tareas.get(i).addDetalle(detaux);
								break;
							}
						}	
					}
				}
				
				break;
			case "4":  //ver detalles de una tarea
				System.out.println("Ver detalles de una tarea");
				System.out.println("Indique el ID de la tarea que desea ver sus detalles");
				int idtarea=sc.nextInt();
				sc.nextLine();
				for(int i=0; i<tareas.size();i++) {
					if(idtarea==tareas.get(i).getId()) {
						aux=tareas.get(i);
						System.out.println(aux.toString());
						for(int j=0; j<aux.getDetalles().size();j++) {
							System.out.println(aux.getDetalles().get(j).toString());
						}
						break;
					}
				}
				
				
				break;
			case "5": //ver todos los detalles	
				System.out.println("Ver todos los detalles");
				for(int i=0; i<tareas.size();i++) {
					aux=tareas.get(i);
					System.out.println(aux.toString());
					for(int j=0; j<aux.getDetalles().size();j++) {
						System.out.println(aux.getDetalles().get(j).toString());
					}
					System.out.println();
				}
				break;
				
			case "6": //realizar un detalle
				System.out.println("Indique id de la tarea que posee el detalle a realizar");
				int idt=sc.nextInt();
				sc.nextLine();
				for(int i=0; i<tareas.size();i++) {
					if(idt==tareas.get(i).getId()) {
					System.out.println(tareas.get(idt).toString());
					for(int j=0; j<tareas.get(idt).getDetalles().size();j++) {
						System.out.println(tareas.get(idt).getDetalles().get(j).toString());
					}
					}}
				System.out.println("Indique id del detalle a realizar");
				int idd=sc.nextInt();
				sc.nextLine();
				if(tareas.get(idt).getDetalles().get(idd).isRealizado())
					System.out.println("El detalle ya est� realizado");
				else {
					gt.realizarDetalle(idd);
					System.out.println("Detalle realizado");
					tareas.get(idt).getDetalles().get(idd).setRealizado(true);
					//detaux=tareas.get(idt).getDetalles().get(idd);
					
					
				}
				
				break;
			case "7": //borrar una tarea y sus detalles
				System.out.println("Borrar una tarea y detalles asociados");
				System.out.println("Introduzca id de la tarea a borrar");
				idt=sc.nextInt();
				sc.nextLine();
				if(idt>tareas.size()) {
					System.out.println("Id fuera de rango");
					break;
				}else {
					if(gt.borrarTareayDetalles(idt)) {
					tareas.get(idt).getDetalles().clear();
					tareas.remove(idt);
					System.out.println("Tarea y detalles borrados");
					}
					
					
				}
				
			case "8":
				
				sc.close();
				if(gt.cerrarConexion()) {
					System.out.println("Conexion cerrada");
					System.out.println("Saliendo");
					fin=true;
				}
				else System.out.println("No se pudo cerrar la conexion");
				
				
			default:
				break;
			}
			
			
			
			
			
		}

	}

}
