import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

public class GestorTareas {
	Connection conexion;
	Statement st;
	ResultSet rs;
	PreparedStatement ps;
	
	
	
	public void conectar() {
		conexion=null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			// TODO: handle exception
			System.out.println("No existe el driver sql");
		}
		try {
			 conexion=DriverManager.getConnection("jdbc:mysql://localhost:3306/tareas","root","");
			 
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public ArrayList<Tarea> cargarDatos(){
		ArrayList <Tarea> tareas=new ArrayList();
		HashMap <Integer, ArrayList<Detalle>> auxdetalles=new HashMap();
		try {
			//st=conexion.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
			st=conexion.createStatement();
			rs=st.executeQuery("SELECT * from detalle;");
			while(rs.next()) {
				if(auxdetalles.containsKey(rs.getInt(2))) {
					auxdetalles.get(rs.getInt(2)).add(new Detalle(rs.getInt(1),rs.getInt(2),rs.getString(3),rs.getBoolean(4)));
				}else {
				auxdetalles.put(rs.getInt(2), new ArrayList());
				auxdetalles.get(rs.getInt(2)).add(new Detalle(rs.getInt(1),rs.getInt(2),rs.getString(3),rs.getBoolean(4)));
					}
				}
			}catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	
		//cargar tareas en arraylist
		try {
			st=conexion.createStatement();
			rs=st.executeQuery("SELECT * from tarea;");
			while(rs.next()) {
				tareas.add(new Tarea(rs.getString(2)));
				
			}
			tareas.stream().forEach(p->{
				if(auxdetalles.containsKey(p.getId())) {
					p.addDetalles(auxdetalles.get(p.getId()));
				}
			});;	
		}catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		
		return tareas;
	}
	
	public boolean nuevaTarea(Tarea tarea) {
		try {
			ps=conexion.prepareStatement("INSERT INTO tarea VALUES( ?,?)");
			ps.setInt(1,tarea.getId());
			ps.setString(2,tarea.getNombre());
			ps.executeUpdate();	
			return true;
		} catch (SQLException e) {
			System.out.println("No se pudo crear la tarea");
			return false;
		}
	}
	
	public boolean nuevoDetalle(Detalle detalle) {
		try {
			ps=conexion.prepareStatement("INSERT INTO detalle VALUES( ?,?,?,?)");
			ps.setInt(1,detalle.getId());
			ps.setInt(2,detalle.getIdtarea());
			ps.setString(3,detalle.getNombre());
			ps.setInt(4, 0);
			ps.executeUpdate();		
			return true;
		} catch (SQLException e) {
			System.out.println("No se pudo crear el detalle");
			return false;
		}
	}
	
	public boolean realizarDetalle(int iddetalle) {
		try {
			st=conexion.createStatement();
			st.executeUpdate("UPDATE detalle SET realizado=TRUE where id_detalle="+iddetalle+";");
			return true;
		} catch (SQLException e) {
			System.out.println("No se pudo realizar el detalle");
			return false;
		}
	}
	
	public boolean borrarTareayDetalles(int idtarea) {
		try {
			st=conexion.createStatement();
			st.executeUpdate("DELETE from detalle where id_tarea="+idtarea+";");
			st.executeUpdate("DELETE from tarea where id="+idtarea+";");
			return true;
		} catch (SQLException e) {
			System.out.println("No se pudo borrar la tarea o los detalles");
			return false;
		}
	}
	public boolean cerrarConexion() {
		try {
			conexion.close();
			return true;
		} catch (SQLException e) {
			return false;
		}
	}

}
