
public class Detalle {
	private static int iddet=0;
	int id;
	int idtarea;
	String nombre;
	boolean realizado;
	
	
	public Detalle() {	}

	public Detalle(int idtarea, String nombre, boolean realizado) {
		super();
		this.id = iddet;
		this.idtarea = idtarea;
		this.nombre = nombre;
		this.realizado = realizado;
		iddet++;
	}
	public Detalle(int id,int idtarea, String nombre, boolean realizado) {
		super();
		this.id = id;
		this.idtarea = idtarea;
		this.nombre = nombre;
		this.realizado = realizado;
		if(this.id>=iddet) iddet=this.id+1;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getIdtarea() {
		return idtarea;
	}
	public void setIdtarea(int idtarea) {
		this.idtarea = idtarea;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public boolean isRealizado() {
		return realizado;
	}
	public void setRealizado(boolean realizado) {
		this.realizado = realizado;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "ID detalle :"+this.id+"\tID tarea: "+this.idtarea+"\tNombre: "+this.nombre+"\tRealizado: "+this.realizado;
	}
	
	

}
